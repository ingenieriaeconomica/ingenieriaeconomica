﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Economica
{
    public partial class Home : Form
    {
       
        public Home()
        {
            InitializeComponent();
           
        }
        int lx, ly;
        private void BtnApplicationExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnMazimezedWindows_Click(object sender, EventArgs e)
        {
            lx = this.Location.X;
            ly = this.Location.Y;

            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            this.btnApplicationRestoreWindows.Visible = true;
            this.btnMazimezedWindows.Visible = false;
        }

        private void BtnMinimizedWindows_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BtnApplicationRestoreWindows_Click(object sender, EventArgs e)
        {
            this.Size = new Size(924, 512);
            this.Location = new Point(lx, ly);
            this.btnApplicationRestoreWindows.Visible = false;
            this.btnMazimezedWindows.Visible = true;
        }
    }
}
