﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economica.formulas
{
    class InteresSimpleFPTI
    {
        // private double Ci,Cf,i,n,it;

        public InteresSimpleFPTI()
        {
        }

        public  double Intereses (double Ci,double i,double n)
            // Interes total a partir de capital inicial , interes % y el periodo

        {

            double IT = 0;
            IT = (Ci * i * n);
            return IT;
        }
        public  double ValorFuturo (double vp,double IT)  /// capita final a  partir del 
        // capital inicial  y el interes total
        {
            double vf = 0;
            vf = vp + IT;
            return vf;
        }

        public  double ValorFuturo(double vp, double i, double n) 
        // capita final a  partir del 
        // capital inicial  , interes  %  , tiempo 
        {
            double vf = 0;
            vf = (vp*(1+i*n));
            return vf;
        }

        public  double ValorPresente_DF(double vf,double i, double n )
        {
            double vp = 0;
            vp = (vf / Math.Pow((1 + i), n));

            return vp; 
        }
    }
}
