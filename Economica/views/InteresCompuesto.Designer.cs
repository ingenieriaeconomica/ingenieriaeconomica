﻿namespace Economica.views
{
    partial class InteresCompuesto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtcapitalFinal = new System.Windows.Forms.TextBox();
            this.txtIntereses = new System.Windows.Forms.TextBox();
            this.txtTasaPorcentual = new System.Windows.Forms.TextBox();
            this.chkIntereses = new System.Windows.Forms.CheckBox();
            this.chkTasa_Porcentual = new System.Windows.Forms.CheckBox();
            this.chkCapital_Final = new System.Windows.Forms.CheckBox();
            this.txtCapitalInicial = new System.Windows.Forms.TextBox();
            this.chkCapita_Inicial = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtcapitalFinal
            // 
            this.txtcapitalFinal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtcapitalFinal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(47)))), ((int)(((byte)(55)))));
            this.txtcapitalFinal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcapitalFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcapitalFinal.ForeColor = System.Drawing.Color.White;
            this.txtcapitalFinal.Location = new System.Drawing.Point(441, 189);
            this.txtcapitalFinal.Name = "txtcapitalFinal";
            this.txtcapitalFinal.Size = new System.Drawing.Size(98, 19);
            this.txtcapitalFinal.TabIndex = 17;
            this.txtcapitalFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtIntereses
            // 
            this.txtIntereses.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIntereses.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(47)))), ((int)(((byte)(55)))));
            this.txtIntereses.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtIntereses.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIntereses.ForeColor = System.Drawing.Color.White;
            this.txtIntereses.Location = new System.Drawing.Point(439, 283);
            this.txtIntereses.Name = "txtIntereses";
            this.txtIntereses.Size = new System.Drawing.Size(100, 19);
            this.txtIntereses.TabIndex = 16;
            this.txtIntereses.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTasaPorcentual
            // 
            this.txtTasaPorcentual.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTasaPorcentual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(47)))), ((int)(((byte)(55)))));
            this.txtTasaPorcentual.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTasaPorcentual.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTasaPorcentual.ForeColor = System.Drawing.Color.White;
            this.txtTasaPorcentual.Location = new System.Drawing.Point(439, 232);
            this.txtTasaPorcentual.Name = "txtTasaPorcentual";
            this.txtTasaPorcentual.Size = new System.Drawing.Size(100, 19);
            this.txtTasaPorcentual.TabIndex = 15;
            this.txtTasaPorcentual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chkIntereses
            // 
            this.chkIntereses.AutoSize = true;
            this.chkIntereses.BackColor = System.Drawing.Color.Transparent;
            this.chkIntereses.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chkIntereses.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIntereses.ForeColor = System.Drawing.Color.White;
            this.chkIntereses.Location = new System.Drawing.Point(262, 287);
            this.chkIntereses.Name = "chkIntereses";
            this.chkIntereses.Size = new System.Drawing.Size(105, 24);
            this.chkIntereses.TabIndex = 14;
            this.chkIntereses.Text = "INTERESES";
            this.chkIntereses.UseVisualStyleBackColor = false;
            // 
            // chkTasa_Porcentual
            // 
            this.chkTasa_Porcentual.AutoSize = true;
            this.chkTasa_Porcentual.BackColor = System.Drawing.Color.Transparent;
            this.chkTasa_Porcentual.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chkTasa_Porcentual.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTasa_Porcentual.ForeColor = System.Drawing.Color.White;
            this.chkTasa_Porcentual.Location = new System.Drawing.Point(262, 237);
            this.chkTasa_Porcentual.Name = "chkTasa_Porcentual";
            this.chkTasa_Porcentual.Size = new System.Drawing.Size(85, 24);
            this.chkTasa_Porcentual.TabIndex = 13;
            this.chkTasa_Porcentual.Text = "TASA %";
            this.chkTasa_Porcentual.UseVisualStyleBackColor = false;
            // 
            // chkCapital_Final
            // 
            this.chkCapital_Final.AutoSize = true;
            this.chkCapital_Final.BackColor = System.Drawing.Color.Transparent;
            this.chkCapital_Final.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chkCapital_Final.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCapital_Final.ForeColor = System.Drawing.Color.White;
            this.chkCapital_Final.Location = new System.Drawing.Point(262, 190);
            this.chkCapital_Final.Name = "chkCapital_Final";
            this.chkCapital_Final.Size = new System.Drawing.Size(137, 24);
            this.chkCapital_Final.TabIndex = 12;
            this.chkCapital_Final.Text = "CAPITAL FINAL";
            this.chkCapital_Final.UseVisualStyleBackColor = false;
            // 
            // txtCapitalInicial
            // 
            this.txtCapitalInicial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCapitalInicial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(47)))), ((int)(((byte)(55)))));
            this.txtCapitalInicial.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCapitalInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCapitalInicial.ForeColor = System.Drawing.Color.White;
            this.txtCapitalInicial.Location = new System.Drawing.Point(439, 139);
            this.txtCapitalInicial.Name = "txtCapitalInicial";
            this.txtCapitalInicial.Size = new System.Drawing.Size(100, 19);
            this.txtCapitalInicial.TabIndex = 11;
            this.txtCapitalInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chkCapita_Inicial
            // 
            this.chkCapita_Inicial.AutoSize = true;
            this.chkCapita_Inicial.BackColor = System.Drawing.Color.Transparent;
            this.chkCapita_Inicial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chkCapita_Inicial.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCapita_Inicial.ForeColor = System.Drawing.Color.White;
            this.chkCapita_Inicial.Location = new System.Drawing.Point(262, 141);
            this.chkCapita_Inicial.Name = "chkCapita_Inicial";
            this.chkCapita_Inicial.Size = new System.Drawing.Size(147, 24);
            this.chkCapita_Inicial.TabIndex = 10;
            this.chkCapita_Inicial.Text = "CAPITAL INICIAL";
            this.chkCapita_Inicial.UseVisualStyleBackColor = false;
            // 
            // InteresCompuesto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtcapitalFinal);
            this.Controls.Add(this.txtIntereses);
            this.Controls.Add(this.txtTasaPorcentual);
            this.Controls.Add(this.chkIntereses);
            this.Controls.Add(this.chkTasa_Porcentual);
            this.Controls.Add(this.chkCapital_Final);
            this.Controls.Add(this.txtCapitalInicial);
            this.Controls.Add(this.chkCapita_Inicial);
            this.Name = "InteresCompuesto";
            this.Text = "InteresCompuesto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtcapitalFinal;
        private System.Windows.Forms.TextBox txtIntereses;
        private System.Windows.Forms.TextBox txtTasaPorcentual;
        private System.Windows.Forms.CheckBox chkIntereses;
        private System.Windows.Forms.CheckBox chkTasa_Porcentual;
        private System.Windows.Forms.CheckBox chkCapital_Final;
        private System.Windows.Forms.TextBox txtCapitalInicial;
        private System.Windows.Forms.CheckBox chkCapita_Inicial;
    }
}